import 'package:flutter/material.dart';

import "pages/home.dart";
import "pages/collection.dart";

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  Map<String, List<String>> _collection = {
    'my collection 01' : ['assets/img1.jpg', 'assets/img2.jpg'], 
    'another collection' :  ['assets/img3.jpg', 'assets/img4.jpg']
  };

  List<String> _simpleCollection = ['food', 'other', 'stuff'];

  void _addOneCollection(String collecName) {
    setState(() {
      // _collection[collecName];
      print('SET STATE MAIN');
      _simpleCollection.add(collecName);
    });
    print(_collection);
  }

  // void _addImageToCollection(String imgName) {
  //   setState(() {
  //     _collection.add(imgName);
  //   });
  //   print(_collection);
  // }

  // void _deleteImageFromCollection(int index) {
  //   setState(() {
  //     _collection.removeAt(index);
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          accentColor: Colors.deepPurple[300],
        ),
        routes: {
          "/": (BuildContext context) => MyHomePage(this._simpleCollection, _addOneCollection),
          "/collection": (BuildContext context) => CollectionPage(),
        });
  }
}
