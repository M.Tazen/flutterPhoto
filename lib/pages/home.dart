import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  // Map<String, List<String>> _collection;

  // MyHomePage(this._collection);
  List<String> _simpleCollection;
  Function addOneCollection;

  MyHomePage(
    this._simpleCollection,
    this.addOneCollection,
  );

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // Map<String, List<String>> _collection;

  List<String> _simpleCollection;
  String _newCollecName;

  Widget _buildCollectionItems(BuildContext context, index) {
    return _collectionButtons(_simpleCollection[index]);
  }

  Widget _buildCollectionList() {
    Widget collectionToShow;

    if (_simpleCollection.length > 0) {
      collectionToShow = ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemBuilder: _buildCollectionItems,
        itemCount: _simpleCollection.length,
      );
    } else {
      collectionToShow = Center(
        child: Text('No collections yet'),
      );
    }
    return collectionToShow;
  }

  
  @override
  void initState() {
    super.initState();
    widget.addOneCollection;
  }


  @override
  Widget build(BuildContext context) {
    _simpleCollection = widget._simpleCollection;
    print("BUILD HOME");
    return Scaffold(
      appBar: AppBar(
        title: Text('FlutterPhoto'),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Center(
                  child: Container(
                    width: 200,
                    margin: EdgeInsets.symmetric(
                      horizontal: 30.0,
                      vertical: 20.0,
                    ),
                    child: TextField(
                      decoration: InputDecoration(
                        fillColor: Theme.of(context).accentColor,
                        labelText: 'New Collection Name',
                      ),
                      onChanged: (String value) {
                        setState(() {
                          this._newCollecName = value;
                        });
                      },
                    ),
                  ),
                ),
                RaisedButton(
                  child: Text('Add'),
                  color: Theme.of(context).accentColor,
                  textColor: Colors.white,
                  onPressed: () {
                    setState(() {
                      widget.addOneCollection(this._newCollecName);
                    });
                    print('add a collection');
                  },
                )
              ],
            ),
            _buildCollectionList(),
          ],
        ),
      ),
    );
  }

  _collectionButtons(String name) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, 'collection');
      },
      child: Container(
        margin: EdgeInsets.only(top: 5.0),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            border: Border.all(color: Color(0xffEEEEEE)),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Color(0xffEEEEEE),
                offset: Offset(3.0, 4.0),
                spreadRadius: 2.0,
                blurRadius: 4.0,
              )
            ],
          ),
          margin: EdgeInsets.symmetric(
            horizontal: 5.0,
            vertical: 5.0,
          ),
          height: 50,
          width: 300,
          padding: EdgeInsets.symmetric(
            horizontal: 20.0,
            vertical: 15.0,
          ),
          child: Text(
            name,
          ),
        ),
      ),
    );
  }
}
